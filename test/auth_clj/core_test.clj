(ns auth-clj.core-test
  (:require [auth-clj.routes :refer [home-page]]
            [clojure.test :refer :all]
            [ring.mock.request :as mock]))

(deftest a-test
  (testing "FIXME, I fail."
    (is (= 0 0))))

(deftest ring-test
  (testing "Test home page"
    (is (= 200 (:status (home-page (mock/request :get "/home")))))))
