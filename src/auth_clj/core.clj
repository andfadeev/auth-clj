(ns auth-clj.core
  (:require [auth-clj.routes :refer [app-routes wrap-user main-pages root-route wrap-menu]]
            [buddy.auth.backends.session :refer [session-backend]]
            [buddy.auth.middleware :refer [wrap-authentication wrap-authorization]]
            [ring.adapter.jetty :refer [run-jetty]]
            [ring.middleware.defaults :refer [wrap-defaults site-defaults]]
            [ring.middleware.stacktrace :refer [wrap-stacktrace]]
            [ring.util.response :as resp]
            [compojure.core :refer :all]))

(defn unauthorized-handler [request metadata]
  (println "metadata2: " metadata)
  (resp/redirect "/login"))

(def auth-backend (session-backend {:unauthorized-handler unauthorized-handler}))

(def app
  (-> (routes app-routes main-pages root-route)
      (wrap-authorization auth-backend)
      (wrap-authentication auth-backend)
      wrap-user
      wrap-menu
      (wrap-defaults site-defaults)
      wrap-stacktrace
      ))

(defn -main
  [& args]
  (run-jetty #'app {:port 3000 :join? false}))
