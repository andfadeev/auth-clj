(defproject auth-clj "0.1.0-SNAPSHOT"
  :description "FIXME: write description"
  :url "http://example.com/FIXME"
  :license {:name "Eclipse Public License"
            :url "http://www.eclipse.org/legal/epl-v10.html"}
  :dependencies [[org.clojure/clojure "1.7.0"]
                 [ring/ring-core "1.4.0"]
                 [ring/ring-jetty-adapter "1.4.0"]
                 [compojure "1.4.0"]
                 [ring/ring-mock "0.3.0"]
                 [buddy "0.8.0"]
                 [ring/ring-devel "1.4.0"]
                 [ring/ring-defaults "0.1.5"]
                 [hiccup "1.0.5"]]
  :main ^:skip-aot auth-clj.core
  :target-path "target/%s"
  :plugins [[lein-ring "0.9.7"]]
  :ring {:handler auth-clj.core/app}
  :profiles {:uberjar {:aot :all}})
