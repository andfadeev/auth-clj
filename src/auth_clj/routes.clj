(ns auth-clj.routes
  (:require [buddy.auth :refer [authenticated? throw-unauthorized]]
            [compojure.core :refer :all]
            [hiccup.form :refer :all]
            [hiccup.page :refer :all]
            [ring.util.anti-forgery :refer [anti-forgery-field]]
            [ring.util.response :as resp]))

(def auth-data {:admin "secret"
                :user "secret"
                :andfadeev "159753"})

(def user-info {:admin {:name "Ivan Ivanov"
                        :age 28}
                :andfadeev {:name "Andrey Fadeev"
                            :age 25}})

(def menu-links [{:href "/admin" :title "Admin"}
                 {:href "/home" :title "Home"}])

(defn logout [r]
  (-> (resp/redirect "/login")
      (assoc :session {})))

(defn login [r]
  (base-layout [:div
                [:h1 "Login"]
                (render-menu (-> r :session :menu))
                (form-to [:post "/login"]
                         (label "username" "Username")
                         (text-field "username")
                         [:br]
                         (label "password" "Password")
                         (password-field "password")
                         [:br]
                         (anti-forgery-field)
                         (submit-button "Login")
                         )]))

(defn handle-login [request]
  (println request)
  (let [username (get-in request [:form-params "username"])
        password (get-in request [:form-params "password"])
        session (:session request)
        found-pwd ((keyword username) auth-data)
        _ (println "handle-login")]
    (if (and found-pwd (= found-pwd password))
      (let [response (-> (resp/redirect "/home")
                         (assoc :session (assoc session :identity (keyword username))))
            _ (println "response: " response)]
        response)
      (resp/redirect "/login")))
  )

(defn base-layout [body]
  (html5 [:body body]))

(defn render-menu [links]
  [:ul (for [link links] [:li [:a {:href (:href link)} (:title link)]])])

(defn home-page [r]
  (println "home page user: " (-> r :session :user))
  (if-not (authenticated? r)
    (throw-unauthorized)
    (html5 [:body
            [:div
             [:h1 "Home page"]
             [:p "This is home page!"]
             [:p (str "Hello, " (-> r :session :user :name))]
             [:a {:href "/logout"} "Logout"]]])))

(defn with-auth [r]
  (if (authenticated? r)
    (println "with user" (:identity r))
    (throw-unauthorized)))

(defn wrap-user [handler]
  (fn [request]
    (println "wrap-user" request)
    (if-let [user-id (-> request :session :identity)
             ]
      (let [_ (println "user-id: " (type user-id))
            user (user-id user-info)]
        (handler (assoc-in request [:session :user] user)))
      (handler request))))

(defroutes app-routes
  (GET "/login" [] login)
  (POST "/login" [] handle-login)
  (GET "/logout" [] logout))


(defn admin-page [request]
  (let [links (-> request :session :menu)]
    (base-layout [:div [:h1 "Admin page"]
                  (render-menu links)
                  [:p "This is admin page."]])))

(defn index-page [request]
  (let [links (-> request :session :menu)]
    (base-layout [:div [:h1 "Index page"]
                  (render-menu (conj links {:href "/login" :title "Log in"}))
                  [:p "This is index page."]])))

(defn wrap-menu [handler]
  (fn [request]
    (println "in wrap-menu")
    (handler (assoc-in request [:session :menu] menu-links))))

(defroutes main-pages
  (GET "/home" [] home-page)
  (GET "/admin" [] admin-page))

(def root-route
  (routes
   (GET "/" [] index-page)))
